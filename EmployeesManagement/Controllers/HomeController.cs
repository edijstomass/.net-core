﻿using EmployeesManagement.Models;
using EmployeesManagement.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesManagement.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository; // == MockEmployeeRepository

        public HomeController(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository; // creates  new MockEmployeeRepository
        }
        public ViewResult Index()
        {
            var model = _employeeRepository.GetAllEmployee();
            return View(model);
        }

        public ViewResult Details()
        {
            HomeDetailsViewModel homeDetailsViewModel = new HomeDetailsViewModel();
            homeDetailsViewModel.Employee = _employeeRepository.GetEmployee(1);
            homeDetailsViewModel.PageTitle = "Employee Details";

            return View(homeDetailsViewModel);
        }





    }
}
