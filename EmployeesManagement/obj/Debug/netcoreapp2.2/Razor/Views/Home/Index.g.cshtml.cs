#pragma checksum "C:\Users\Eddy\Desktop\CORE ASP.NET\EmployeesManagement\EmployeesManagement\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "32ae89348a850528b802d59fd2e8dda17cfc9d40"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"32ae89348a850528b802d59fd2e8dda17cfc9d40", @"/Views/Home/Index.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<EmployeesManagement.Models.Employee>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(57, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\Eddy\Desktop\CORE ASP.NET\EmployeesManagement\EmployeesManagement\Views\Home\Index.cshtml"
   
    Layout = "~/Views/Shared/_Layout.cshtml";
    ViewBag.Title = "Employee List";

#line default
#line hidden
            BeginContext(152, 69, true);
            WriteLiteral("\r\n\r\n\r\n    <table>\r\n\r\n        <thead>                                 ");
            EndContext();
            BeginContext(235, 50, true);
            WriteLiteral("\r\n            <tr>                                ");
            EndContext();
            BeginContext(292, 50, true);
            WriteLiteral("\r\n                <th>ID</th>                     ");
            EndContext();
            BeginContext(366, 50, true);
            WriteLiteral("\r\n                <th>Name</th>                   ");
            EndContext();
            BeginContext(440, 50, true);
            WriteLiteral("\r\n                <th>Department</th>             ");
            EndContext();
            BeginContext(514, 60, true);
            WriteLiteral("\r\n\r\n            </tr>\r\n        </thead>\r\n\r\n        <tbody>\r\n");
            EndContext();
#line 22 "C:\Users\Eddy\Desktop\CORE ASP.NET\EmployeesManagement\EmployeesManagement\Views\Home\Index.cshtml"
             foreach (var employee in Model)
            {

#line default
#line hidden
            BeginContext(635, 47, true);
            WriteLiteral("            <tr>                               ");
            EndContext();
            BeginContext(689, 22, true);
            WriteLiteral("\r\n                <td>");
            EndContext();
            BeginContext(712, 11, false);
#line 25 "C:\Users\Eddy\Desktop\CORE ASP.NET\EmployeesManagement\EmployeesManagement\Views\Home\Index.cshtml"
               Write(employee.Id);

#line default
#line hidden
            EndContext();
            BeginContext(723, 15, true);
            WriteLiteral("</td>          ");
            EndContext();
            BeginContext(808, 27, true);
            WriteLiteral("     \r\n                <td>");
            EndContext();
            BeginContext(836, 13, false);
#line 26 "C:\Users\Eddy\Desktop\CORE ASP.NET\EmployeesManagement\EmployeesManagement\Views\Home\Index.cshtml"
               Write(employee.Name);

#line default
#line hidden
            EndContext();
            BeginContext(849, 27, true);
            WriteLiteral("</td>\r\n                <td>");
            EndContext();
            BeginContext(877, 19, false);
#line 27 "C:\Users\Eddy\Desktop\CORE ASP.NET\EmployeesManagement\EmployeesManagement\Views\Home\Index.cshtml"
               Write(employee.Department);

#line default
#line hidden
            EndContext();
            BeginContext(896, 26, true);
            WriteLiteral("</td>\r\n            </tr>\r\n");
            EndContext();
#line 29 "C:\Users\Eddy\Desktop\CORE ASP.NET\EmployeesManagement\EmployeesManagement\Views\Home\Index.cshtml"
            }

#line default
#line hidden
            BeginContext(937, 38, true);
            WriteLiteral("        </tbody>\r\n\r\n    </table>\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<EmployeesManagement.Models.Employee>> Html { get; private set; }
    }
}
#pragma warning restore 1591
